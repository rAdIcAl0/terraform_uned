#! /bin/bash
# Instalacion de Ansible y Git
sudo apt-get update
sudo apt-get install ansible -y
sudo apt-get install git -y

# Descarga del role de Ansible para la instalacion de PLEX Media Server
cd /home/ubuntu
git clone https://gitlab.com/rAdIcAl0/ansible_plex_uned.git
sudo cp -rp ansible_plex_uned/* /etc/ansible
sudo ansible-playbook -i hosts /etc/ansible/playbook.yml

