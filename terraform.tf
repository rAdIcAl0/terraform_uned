provider "aws" {
    region = "eu-central-1"
    access_key = "AKIAU4UZM6QARS547W6L"
    secret_key = "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"
}

# VPC
resource "aws_vpc" "vpc1" {
  cidr_block       = "192.168.0.0/16"
 tags = { 
    Name = "vpc1"
}
}

# GATEWAY
resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.vpc1.id}"
}

#Route table
resource "aws_route_table" "prod_route_table" {
  vpc_id = aws_vpc.vpc1.id

  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.gw.id
    }

  route {
      ipv6_cidr_block = "::/0"
      gateway_id = aws_internet_gateway.gw.id
    }

  tags = {
    Name = "route_table"
  }
}

#Create Subnet

resource "aws_subnet" "subnet-privada" {
  vpc_id = "${aws_vpc.vpc1.id}"
  cidr_block = "192.168.1.0/24"
  availability_zone = "eu-central-1a"
 tags = {
    Name = "subnet-privada"
}
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet-privada.id
  route_table_id = aws_route_table.prod_route_table.id
}


#Security group

resource "aws_security_group" "allow_ssh_http" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.vpc1.id

 ingress {
      description      = "SSH"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
}
 ingress {
      description      = "Servidor PLEX"
      from_port        = 32400
      to_port          = 32400
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
}
 ingress {
      description      = "PLEX Companion"
      from_port        = 3005
      to_port          = 3005
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
}
 ingress {
      description      = "Descubrimiento de red"
      from_port        = 5353
      to_port          = 5353
      protocol         = "udp"
      cidr_blocks      = ["0.0.0.0/0"]
}
 ingress {
      description      = "PLEX Roku"
      from_port        = 8324
      to_port          = 8324
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
}
 ingress {
      description      = "Descubrimiendo de red UDP"
      from_port        = 32410
      to_port          = 32414
      protocol         = "udp"
      cidr_blocks      = ["0.0.0.0/0"]
}
 ingress {
      description      = "PLEX DLNA"
      from_port        = 1900 
      to_port          = 1900
      protocol         = "udp"
      cidr_blocks      = ["0.0.0.0/0"]
}
 ingress {
      description      = "Servidor DLNA PLEX"
      from_port        = 32469
      to_port          = 32469
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
}
 ingress {
      description      = "RDP"
      from_port        = 3389
      to_port          = 3389
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
}
 ingress { 
      description      = "HTTP"
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
}
 egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
}

  tags = {
    Name = "allow_ssh_http"
  }
}

#Network interface

resource "aws_network_interface" "interface1" {
  subnet_id       = aws_subnet.subnet-privada.id
  private_ips     = ["192.168.1.30"]
  security_groups = [aws_security_group.allow_ssh_http.id]
}


#EIP

resource "aws_eip" "eip1" {
  vpc                       = true
  network_interface         = aws_network_interface.interface1.id
  associate_with_private_ip = "192.168.1.30"
  depends_on = [aws_internet_gateway.gw]
}


resource "aws_instance" "ansible" {
    ami = "ami-0a733a350142f8f81"
    instance_type = "t2.medium"
    availability_zone = "eu-central-1a"
    key_name = "aws1"
    user_data = "${file("post.sh")}"
    network_interface {
      network_interface_id = aws_network_interface.interface1.id
      device_index = 0
        }
}
